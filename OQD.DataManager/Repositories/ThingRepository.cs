﻿using OQD.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OQD.DataManager.Repositories
{
    public class ThingRepository
    {
        OQD.Data.OQD_DEVEntities context = new OQD_DEVEntities();

        public async Task<List<Thing>> ListAsync()
        {
            return await context.Things.ToListAsync();
        }

        public async Task<Thing> GetById(int id)
        {
            return await context.Things.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task SaveAsync(Thing thing)
        {
            var _thing = await GetById(thing.Id);

            if (_thing.Id == 0)
            {
                context.Things.Add(thing);
                await context.SaveChangesAsync();
                return;
            }

            _thing.Name = thing.Name;
            _thing.DateAdded = thing.DateAdded;
            _thing.IsPlace = thing.IsPlace;
            _thing.PhotoUrl = thing.PhotoUrl;
            _thing.Tags = thing.Tags;
            _thing.UserId = thing.UserId;

            await context.SaveChangesAsync();

        }
    }
}

﻿using OQD.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OQD.DataManager.Repositories
{
    public class ThingPlaceRepository
    {
        OQD.Data.OQD_DEVEntities context = new OQD_DEVEntities();

        public async Task<List<ThingPlace>> ListAsync()
        {
            return await context.ThingPlaces.ToListAsync();
        }

        public async Task<ThingPlace> GetById(int id)
        {
            return await context.ThingPlaces.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task SaveAsync(ThingPlace thingPlace)
        {
            var _thingPlace = await GetById(thingPlace.Id);

            if (_thingPlace.Id == 0)
            {
                context.ThingPlaces.Add(thingPlace);
                await context.SaveChangesAsync();
                return;
            }

            _thingPlace.PlaceId = thingPlace.PlaceId;
            _thingPlace.ThingId = thingPlace.ThingId;
            


            await context.SaveChangesAsync();

        }
    }
}

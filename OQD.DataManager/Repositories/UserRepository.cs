﻿using OQD.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OQD.DataManager.Repositories
{
    public class UserRepository
    {
        OQD.Data.OQD_DEVEntities context = new OQD_DEVEntities();
        
        public async Task<List<User>> ListAsync()
        {
            return await context.Users.ToListAsync();
        }

        public async Task<User> GetById(int id)
        {
            return await context.Users.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task SaveAsync(User user)
        {
            var _user = await GetById(user.Id);

            if (_user.Id == 0)
            {
                context.Users.Add(user);
                await context.SaveChangesAsync();
                return;
            }

            _user.Name = user.Name;
            _user.Password = user.Password;
            _user.PhotoUrl = user.PhotoUrl;
            _user.UserName = user.UserName;

            await context.SaveChangesAsync();

        }
    }
}

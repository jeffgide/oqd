﻿using OQD.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OQD.DataManager.Repositories
{
    public class UserSettingsRepository
    {
        OQD.Data.OQD_DEVEntities context = new OQD_DEVEntities();

        public async Task<List<UserSetting>> ListAsync()
        {
            return await context.UserSettings.ToListAsync();
        }

        public async Task<UserSetting> GetById(int id)
        {
            return await context.UserSettings.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task SaveAsync(UserSetting userSettings)
        {
            var _userSettings = await GetById(userSettings.Id);

            if (_userSettings.Id == 0)
            {
                context.UserSettings.Add(userSettings);
                await context.SaveChangesAsync();
                return;
            }

            _userSettings.PlaceQtyLimit = userSettings.PlaceQtyLimit;
            _userSettings.ThingQtyLimit = userSettings.ThingQtyLimit;
            _userSettings.UserId = userSettings.UserId;
            
            await context.SaveChangesAsync();

        }
    }
}

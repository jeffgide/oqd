﻿using OQD.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OQD.DataManager.Repositories
{
    public class PlaceRepository
    {
        OQD.Data.OQD_DEVEntities context = new OQD_DEVEntities();

        public async Task<List<Place>> ListAsync()
        {
            return await context.Places.ToListAsync();
        }

        public async Task<Place> GetById(int id)
        {
            return await context.Places.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task SaveAsync(Place place)
        {
            var _place = await GetById(place.Id);

            if (_place.Id == 0)
            {
                context.Places.Add(place);
                await context.SaveChangesAsync();
                return;
            }

            _place.Name = place.Name;
            _place.DateAdded = place.DateAdded;
            _place.PhotoUrl = place.PhotoUrl;
            _place.Tags = place.Tags;
            _place.UserId = place.UserId;
            _place.Latitude = place.Latitude;
            _place.Longitude = place.Longitude;

            await context.SaveChangesAsync();

        }
    }
}

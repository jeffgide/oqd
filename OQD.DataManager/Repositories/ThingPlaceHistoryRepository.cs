﻿using OQD.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OQD.DataManager.Repositories
{
    public class ThingPlaceHistoryRepository
    {
        OQD.Data.OQD_DEVEntities context = new OQD_DEVEntities();

        public async Task<List<ThingPlaceHistory>> ListAsync()
        {
            return await context.ThingPlaceHistories.ToListAsync();
        }

        public async Task<ThingPlaceHistory> GetById(int id)
        {
            return await context.ThingPlaceHistories.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task SaveAsync(ThingPlaceHistory thingPlaceHistory)
        {
            var _thingPlaceHistory = await GetById(thingPlaceHistory.Id);

            if (_thingPlaceHistory.Id == 0)
            {
                context.ThingPlaceHistories.Add(thingPlaceHistory);
                await context.SaveChangesAsync();
                return;
            }

            _thingPlaceHistory.ThingPlaceId = thingPlaceHistory.ThingPlaceId;
            _thingPlaceHistory.Obs = thingPlaceHistory.Obs;
            _thingPlaceHistory.Date = thingPlaceHistory.Date;



            await context.SaveChangesAsync();

        }
    }
}

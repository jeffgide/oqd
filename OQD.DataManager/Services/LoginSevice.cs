﻿using OQD.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OQD.DataManager.Services
{
    public class LoginSevice
    {
        OQD.Data.OQD_DEVEntities context = new OQD_DEVEntities();

        public async Task<bool>Login(User user)
        {
            return await context.Users.Where(x => x.UserName == user.UserName && x.Password == x.Password).AnyAsync();
        }

    }
}
